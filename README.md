# Contributing to the HAEQS Site

## Installing git and getting access to GitLab

Git is a version control software. How you install it depends on your system. For Ubuntu Linux you can simply 
type in a terminal:
```shell
$ sudo apt install git
```

To contribute to the web site you need an account on gitlab.com. If you don't have one already, you can create
a new account under https://gitlab.com/users/sign_in.

You need to be in the haeqs group. Once you have created a gitlab account and signed in, you can request to be
part of the group under https://gitlab.com/groups/haeqs.

Once your gitlab account is all set up you can get a local copy of the haeqs web site by typing
```shell
$ git clone git@gitlab.com:haeqs/haeqs.gitlab.io.git
```

## Installing lektor

Lektor is the editing system we use to keep the process of eding the website as simple as possible. It is a 
static site generator and GUI (graphic user interface) server that allow you to edit pages without any HTML 
knowledge. Goal later is to host the lektor server so that someone can simply sign-in and make edits and hit save to 
publish directly to the live site. 

If you have Python installed on your computer (pretty much everybody has this) you should be able to install 
lektor by typing
```shell 
$ sudo pip install lektor
 ```
If you get an error message like "pip: command not found" you might need to install pip first:
```shell
$ sudo apt install python-pip
```
(on Ubuntu). If that does not work either or you want to go your own way, follow the instructions on 
https://www.getlektor.com/downloads/

## Edit page

To get lektor running, go to the directory containing the web site code and start lektor, e.g.
```shell
$ cd haeqs.gitlab.io
$ lektor server
```
lektor runs in your browser. Go to http://localhost:5000/ to see the haeqs website. There is an edit button
in the top right of the page which you can use to edit the page.

For more in-depth help on how to edit pages, the lektor documentation is actually pretty good: 
https://www.getlektor.com/docs/quickstart/

If you know what templates are and want to edit the site's templates: the templating language of Lektor is 
Jinja2 (http://jinja.pocoo.org/)

## Deploying your changes to the actual web site

If you are finished editing you need to "commit" you changes to tell git what has changed. From the haeqs.gitlab.io
directory, do
```shell
$ git commit -m "<HERE GOES A DESCRIPTION WHAT YOU HAVE CHANGED>" .
```
(Notice the lonely dot at the end of the line)

(Of course you should fill out the description. Do not just copy the text please! :-))

(If you use git for the first time it will ask you for a name and an email. It will print instructions how to
to this, just follow them.)

Now you local git repository knows about your changes, but you still have to tell the gitlab repository about 
them. You do this by typing 
```shell
$ git push origin master
```
When you push, the task triggers a build that publishes the changes to the live site. We are using GitLabCI to 
do this and the configuration is in .gitlab-ci.yml (instructions here: 
https://www.getlektor.com/docs/deployment/glpages/). 

The publishing can take several minutes to complete and to see if it's running you can, go here: 
https://gitlab.com/haeqs/haeqs.gitlab.io/pipelines. (We thought it was broken when it said "pending", but it 
was just waiting for a publicly-available GitLabCI runner to become available. So have some patience!)

## Integrating the changes other people made to your local copy

We are doing this complicated process with git mostly for one reason: If you make a change and somebody else 
makes another change, without git these changes would overwrite each other and one edit would be lost. So, 
every time before you start making an edit to the web site you need to update your local copy like this:
```shell
$ git pull
```
Don't forget to do this or else you will get into trouble when pushing and it is beyond the scope of this 
document to help you out of that trouble!
 
## Updating this documentation

If anything is unclear in this documentation, you can improve it because it is in git too! Just look for the 
file README.md under the haeqs.gitlab.io directory and make the necessary changes, then commit and push as
usual.
